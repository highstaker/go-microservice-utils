package interfaces

type MongoClientFilter struct {
	Key   string
	Value interface{} // should correspond to database type. Pass int64 for Int64, string for String, etc
}
