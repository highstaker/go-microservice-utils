package logger

import (
	"fmt"

	commonInterfaces "gitlab.com/highstaker/go-microservice-utils/pkg/common/interfaces"
)

type Logger struct {
}

func New() *Logger {
	return &Logger{}
}

//TODO: move to zerolog or something and get rid of `fmt` calls

func (l *Logger) Info(v ...any) {
	fmt.Println(v...)
}

func (l *Logger) Infof(format string, v ...any) {
	fmt.Print(format, ":")
	fmt.Println(v...)
}

func (l *Logger) Error(v ...any) {
	fmt.Println(v...)
}

func (l *Logger) Errorf(format string, v ...any) {
	fmt.Print(format, ":")
	fmt.Println(v...)
}

func (l *Logger) Fatal(v ...any) {
	fmt.Println(v...)
}

func (l *Logger) Fatalf(format string, v ...any) {
	fmt.Print(format, ":")
	fmt.Println(v...)
}

func (l *Logger) WithErr(err error) commonInterfaces.Logger {
	//FIXME
	fmt.Print("Error: ", err.Error(), "; ")

	//TODO
	return l
}
