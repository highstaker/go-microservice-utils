
check-compile:
	go vet ./...

test:
	MONGO_DSN="mongodb://exampleuser:examplepass@172.17.0.4:27017/test_exampledb?connect=direct&ssl=false&authSource=admin" \
	go test ./... -coverprofile cover.out.tmp
	# chain `grep -v` with paths you want to exclude. This trick was found here https://habr.com/ru/articles/721958/
	cat cover.out.tmp | grep -v "storage/mongodb/errors.go" > cover.out 
	rm cover.out.tmp
	go tool cover -html cover.out -o coverout.html
