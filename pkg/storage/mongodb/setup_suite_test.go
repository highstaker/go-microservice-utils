package mongodb

import (
	"context"
	"os"
	"testing"

	"github.com/stretchr/testify/suite"
)

type TestSuite struct {
	suite.Suite

	ctx context.Context

	repo *MongoClient
}

func (ts *TestSuite) SetupSuite() {
	ts.ctx = context.Background()

	cfg := &Config{
		DSN:               os.Getenv("MONGO_DSN"),
		MigrationLocation: "file://test_migrations",
	}

	repo, err := New(ts.ctx, cfg)
	if err != nil {
		ts.T().Fatal(err.Error())
	}
	ts.repo = repo

	ts.clean()
}

func TestRunTestSuite(t *testing.T) {
	suite.Run(t, &TestSuite{})
}

// TODO: delete
// func (ts *TestSuite) TestFail() {
// 	ts.Fail("LOLQEQ")
// }
