package dummyserver

import (
	"context"
	"fmt"
	"time"
)

type Dummyserver struct {
	ctx       context.Context
	canceller context.CancelFunc
}

func New() *Dummyserver {
	ctx, cancelFunc := context.WithCancel(context.Background())

	return &Dummyserver{
		ctx:       ctx,
		canceller: cancelFunc,
	}
}

func (s *Dummyserver) Start() error {
	go func() {
		var count uint64

	mainloop:
		for {
			select {
			case <-s.ctx.Done():
				break mainloop
			default:
				fmt.Println("Doing something: ", count)
				count++
				time.Sleep(time.Second * 1)
			}
		}
	}()

	return nil
}

func (s *Dummyserver) Stop() error {
	s.canceller()

	return nil
}

func (s *Dummyserver) GetObligatoryStart() bool {
	return true
}

func (s *Dummyserver) GetName() string {
	return "Dummy Server"
}
