package application

import (
	"context"
	"fmt"

	"github.com/caarlos0/env/v6"

	commonInterfaces "gitlab.com/highstaker/go-microservice-utils/pkg/common/interfaces"
	utilsLogger "gitlab.com/highstaker/go-microservice-utils/pkg/logger"
	dummyserver "gitlab.com/highstaker/go-microservice-utils/pkg/servers/dummy_server"
	grpcserver "gitlab.com/highstaker/go-microservice-utils/pkg/servers/grpc_server"
	"gitlab.com/highstaker/go-microservice-utils/pkg/storage/mongodb"
)

func initSystemContext(app *Application) {

	// TODO: better context that can track signals and shut the service down gracefully
	ctx := context.Background()

	app.systemContext = ctx
}

func initLogger(app *Application) {
	app.logger = utilsLogger.New()
}

func initServers(app *Application, cfg *Config) {
	servers := make(map[string]commonInterfaces.Server)

	//TODO: server config validation. Maybe with `github.com/go-playground/validator/v10`

	//TODO: remove DummyServer later
	dummyServer := dummyserver.New()
	servers[dummyServer.GetName()] = dummyServer

	grpcServer := grpcserver.New(cfg.GrpcServer, app.GetLogger())
	servers[grpcServer.GetName()] = grpcServer

	app.servers = servers
}

func initConfig() (*Config, error) {
	cfg := NewConfig()

	err := env.Parse(cfg)
	if err != nil {
		return nil, fmt.Errorf("failed to parse config: %w", err)
	}

	return cfg, nil
}

func initStorage(app *Application, cfg *Config) error {

	//make optional
	mongoClient, err := mongodb.New(app.GetSystemContext(), cfg.MongoDB)
	if err != nil {
		return fmt.Errorf("could not create mongo client: %w", err)
	}
	app.mongoClient = mongoClient

	return nil
}
