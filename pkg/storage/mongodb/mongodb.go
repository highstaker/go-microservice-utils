package mongodb

import (
	"context"
	"errors"
	"fmt"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/mongo/driver/connstring"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/mongodb"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

type MongoClient struct {
	client *mongo.Client

	db *mongo.Database
}

func New(ctx context.Context, cfg *Config) (*MongoClient, error) {
	connstring, err := connstring.Parse(cfg.DSN)
	if err != nil {
		return nil, fmt.Errorf("failed to parse mongo connection string: %w", err)
	}

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(cfg.DSN))
	if err != nil {
		return nil, fmt.Errorf("failed to connect to mongo: %w", err)
	}
	if err := client.Ping(ctx, nil); err != nil {
		return nil, fmt.Errorf("failure on mongo ping: %w", err)
	}

	m, err := migrate.New(cfg.MigrationLocation, cfg.DSN)
	if err != nil {
		return nil, fmt.Errorf("failed to start migration: %w", err)
	}
	if err := m.Up(); err != nil && !errors.Is(err, migrate.ErrNoChange) {
		return nil, fmt.Errorf("failed to up migrations: %w", err)
	}
	if errSrc, errDB := m.Close(); errSrc != nil || errDB != nil {
		return nil, fmt.Errorf("failed to close migrations: source error: %w; DB error: %w", errSrc, errDB)
	}

	return new(client, connstring.Database), nil
}

func new(client *mongo.Client, dbName string) *MongoClient {
	return &MongoClient{
		client: client,

		db: client.Database(dbName),
	}
}

// TODO: don't forget to use on shutdown!
func (cl *MongoClient) Close(ctx context.Context) error {
	return cl.client.Disconnect(ctx)
}

func (cl *MongoClient) GetDatabase(ctx context.Context) *mongo.Database {
	return cl.db
}

func (cl *MongoClient) GetCollectionByName(collectionName string) *mongo.Collection {
	return cl.db.Collection(collectionName)
}
