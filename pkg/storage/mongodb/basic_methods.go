package mongodb

import (
	"context"
	"errors"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	commonInterfaces "gitlab.com/highstaker/go-microservice-utils/pkg/common/interfaces"
)

//TODO: integration tests for all these

func (cl *MongoClient) GetOne(ctx context.Context, collectionName string, destStruct interface{}, filters ...commonInterfaces.MongoClientFilter) error {
	filter := bson.D{}
	for _, f := range filters {
		filter = append(filter, bson.E{Key: f.Key, Value: f.Value})
	}

	coll := cl.GetCollectionByName(collectionName)

	err := coll.FindOne(ctx, filter).Decode(destStruct)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return ErrNotFound
		}
		return fmt.Errorf("failed to find: %w", err)
	}

	return nil
}

func (cl *MongoClient) GetCount(ctx context.Context, collectionName string, filters ...commonInterfaces.MongoClientFilter) (int64, error) {
	filter := bson.D{}
	for _, f := range filters {
		filter = append(filter, bson.E{Key: f.Key, Value: f.Value})
	}

	coll := cl.GetCollectionByName(collectionName)

	count, err := coll.CountDocuments(ctx, filter)
	if err != nil {
		return 0, fmt.Errorf("failed to find: %w", err)
	}

	return count, nil
}

func (cl *MongoClient) GetMany() {
	//TODO

}

func (cl *MongoClient) GetManyStream() {
	//TODO

}

func (cl *MongoClient) GetManyPaginated() {
	//TODO

}

func (cl *MongoClient) GetManyStreamPaginated() {
	//TODO

}

func (cl *MongoClient) Insert(ctx context.Context, collectionName string, docsToInsert []interface{}) (int64, error) {
	coll := cl.GetCollectionByName(collectionName)

	ids, err := coll.InsertMany(ctx, docsToInsert)
	count := int64(len(ids.InsertedIDs))

	if err != nil {
		return count, fmt.Errorf("failed to insert: %w", err)
	}

	return count, nil
}
