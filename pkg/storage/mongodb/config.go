package mongodb

type Config struct {
	// DSN is a connection string for Mongo
	DSN string `env:"DSN"`
	// MigrationLocation - is the path to folder with migration files
	MigrationLocation string `env:"MIGRATION_LOCATION"`
}
