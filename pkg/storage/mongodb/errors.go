package mongodb

import "errors"

var (
	ErrNotFound = errors.New("not found")
	// ErrInvalidInputType = errors.New("invalid incoming document type")
)

func (cl MongoClient) IsErrNotFound(err error) bool {
	return errors.Is(err, ErrNotFound)
}
