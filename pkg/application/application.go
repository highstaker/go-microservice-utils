package application

import (
	"context"
	"fmt"

	commonInterfaces "gitlab.com/highstaker/go-microservice-utils/pkg/common/interfaces"
)

type Application struct {
	systemContext context.Context
	logger        commonInterfaces.Logger

	servers map[string]commonInterfaces.Server

	mongoClient commonInterfaces.MongoClient
}

func New() (*Application, error) {
	app := &Application{}

	cfg, err := initConfig()
	if err != nil {
		return nil, fmt.Errorf("failed to initialize config: %w", err)
	}

	initSystemContext(app)
	initLogger(app)

	err = initStorage(app, cfg)
	if err != nil {
		return nil, fmt.Errorf("failed to initialize storage: %w", err)
	}

	initServers(app, cfg)

	return app, nil
}

func (a *Application) Run() error {

	for _, serv := range a.servers {
		if err := serv.Start(); err != nil {
			if serv.GetObligatoryStart() {
				a.logger.WithErr(err).Fatalf("failed to start server %s", serv.GetName())
			}
			a.logger.WithErr(err).Errorf("failed to start server %s", serv.GetName())
		}
	}

	a.logger.Info("application running")
	// this is where it halts during normal running.
	<-a.systemContext.Done()
	a.logger.Info("system context interrupted, stopping the application")

	//TODO: stop in parallel
	for _, serv := range a.servers {
		if err := serv.Stop(); err != nil {
			a.logger.WithErr(err).Errorf("failed to stop server %s", serv.GetName())
		}
	}

	return nil
}

func (a *Application) GetSystemContext() context.Context {
	return a.systemContext
}

func (a *Application) GetLogger() commonInterfaces.Logger {
	return a.logger
}

func (a *Application) GetServerByName(name string) commonInterfaces.Server {
	return a.servers[name]
}

func (a *Application) GetMongoClient() commonInterfaces.MongoClient {
	return a.mongoClient
}
