package application

import (
	serverConfig "gitlab.com/highstaker/go-microservice-utils/pkg/servers/config"
	"gitlab.com/highstaker/go-microservice-utils/pkg/storage/mongodb"
)

type Config struct {
	GrpcServer *serverConfig.ServerConfig `envPrefix:"GRPC_SERVER_"`

	MongoDB *mongodb.Config `envPrefix:"MONGO_"`
}

func NewConfig() *Config {
	//TODO: optionally disable a server and make its config non-required

	return &Config{
		GrpcServer: &serverConfig.ServerConfig{},

		MongoDB: &mongodb.Config{},
	}
}
