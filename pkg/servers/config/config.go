package config

type ServerConfig struct {
	// Host is hostname or IP of this server
	Host string `env:"HOST" envDefault:"0.0.0.0"`
	// Host is port where this server is hosted
	Port string `env:"PORT" envDefault:"8888"`

	// Obligatory defines, whether the program should crash if the server failed to start
	Obligatory bool `env:"OBLIGATORY" envDefault:"true"`
}
