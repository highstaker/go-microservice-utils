package mongodb

import (
	"context"

	commonInterfaces "gitlab.com/highstaker/go-microservice-utils/pkg/common/interfaces"
	"go.mongodb.org/mongo-driver/bson"
)

type ModelTest struct {
	ID      string `bson:"id"`
	GroupID int64  `bson:"group_id"`
}

const (
	collectionName = "examplecollection"
)

func (ts *TestSuite) TestGetOne() {
	testcases := []struct {
		name string

		preinserted []ModelTest
		wantedID    string

		expResult ModelTest
		expErr    error
	}{
		{
			name: "nonexistent_entry",

			preinserted: []ModelTest{},
			wantedID:    "a",

			expResult: ModelTest{
				ID:      "a",
				GroupID: 1,
			},
			expErr: ErrNotFound,
		}, //testcase
		{
			name: "existing_entry",

			preinserted: []ModelTest{
				{
					ID:      "a",
					GroupID: 1,
				},
				{
					ID:      "b",
					GroupID: 2,
				},
			},
			wantedID: "b",

			expResult: ModelTest{
				ID:      "b",
				GroupID: 2,
			},
			expErr: nil,
		}, //testcase
	}

	for _, tc := range testcases {
		ts.Run(tc.name, func() {
			for _, m := range tc.preinserted {
				ts.insert(m)
			}

			res := ModelTest{}
			err := ts.repo.GetOne(ts.ctx, collectionName, &res, commonInterfaces.MongoClientFilter{Key: "id", Value: tc.wantedID})
			ts.ErrorIs(err, tc.expErr)
			if err == nil {
				ts.Equal(tc.expResult, res, "inequality")
			}

			ts.clean()
		})
	}
}

func (ts *TestSuite) TestGetCount() {
	testcases := []struct {
		name string

		preinserted   []ModelTest
		filterGroupID int64

		expResult int64
	}{
		{
			name: "no_entries",

			preinserted: []ModelTest{},

			expResult: 0,
		}, //testcase
		{
			name: "one_entry",

			preinserted: []ModelTest{
				{
					ID:      "a",
					GroupID: 1,
				},
			},

			expResult: 1,
		}, //testcase
		{
			name: "multiple_entries",

			preinserted: []ModelTest{
				{
					ID:      "a",
					GroupID: 1,
				},
				{
					ID:      "b",
					GroupID: 2,
				},
				{
					ID:      "c",
					GroupID: 3,
				},
				{
					ID:      "d",
					GroupID: 3,
				},
				{
					ID:      "e",
					GroupID: 1,
				},
			},

			expResult: 5,
		}, //testcase
		{
			name: "multiple_entries_filtered",

			preinserted: []ModelTest{
				{
					ID:      "a",
					GroupID: 1,
				},
				{
					ID:      "b",
					GroupID: 2,
				},
				{
					ID:      "c",
					GroupID: 3,
				},
				{
					ID:      "d",
					GroupID: 3,
				},
				{
					ID:      "e",
					GroupID: 1,
				},
			},
			filterGroupID: 3,

			expResult: 2,
		}, //testcase
	}

	for _, tc := range testcases {
		ts.Run(tc.name, func() {
			for _, m := range tc.preinserted {
				ts.insert(m)
			}

			filters := make([]commonInterfaces.MongoClientFilter, 0, 1)
			if tc.filterGroupID != 0 {
				filters = append(filters, commonInterfaces.MongoClientFilter{Key: "group_id", Value: tc.filterGroupID})
			}

			count, err := ts.repo.GetCount(ts.ctx, collectionName, filters...)
			ts.NoError(err, "unexpected error")
			if err == nil {
				ts.Equal(tc.expResult, count, "inequality")
			}

			ts.clean()
		})
	}
}

func (ts *TestSuite) TestInsert() {
	testcases := []struct {
		name string

		preinserted []ModelTest

		toInsert []ModelTest

		expResult    int64
		errorHappens bool
	}{
		{
			name: "empty_db_insert_one",

			toInsert: []ModelTest{
				{
					ID:      "a",
					GroupID: 1,
				},
			},

			expResult:    1,
			errorHappens: false,
		}, //testcase
		{
			name: "empty_db_insert_several",

			toInsert: []ModelTest{
				{
					ID:      "a",
					GroupID: 1,
				},
				{
					ID:      "b",
					GroupID: 2,
				},
			},

			expResult:    2,
			errorHappens: false,
		}, //testcase
		{
			name: "non-empty_db_insert_several",

			preinserted: []ModelTest{
				{
					ID:      "c",
					GroupID: 1,
				},
				{
					ID:      "d",
					GroupID: 2,
				},
			},

			toInsert: []ModelTest{
				{
					ID:      "a",
					GroupID: 1,
				},
				{
					ID:      "b",
					GroupID: 2,
				},
			},

			expResult:    2,
			errorHappens: false,
		}, //testcase
		{
			name: "non-empty_db_insert_several_conflict",

			preinserted: []ModelTest{
				{
					ID:      "c",
					GroupID: 1,
				},
				{
					ID:      "d",
					GroupID: 2,
				},
			},

			toInsert: []ModelTest{
				{
					ID:      "a",
					GroupID: 1,
				},
				{
					ID:      "d",
					GroupID: 2,
				},
				{
					ID:      "b",
					GroupID: 2,
				},
			},

			expResult:    2,
			errorHappens: true,
		}, //testcase
	}

	for _, tc := range testcases {
		ts.Run(tc.name, func() {
			for _, m := range tc.preinserted {
				ts.insert(m)
			}

			// a canonical way, can't do anything better.
			// https://github.com/golang/go/wiki/InterfaceSlice#what-can-i-do-instead
			toInsert := make([]interface{}, 0, len(tc.toInsert))
			for _, d := range tc.toInsert {
				toInsert = append(toInsert, d)
			}

			count, err := ts.repo.Insert(ts.ctx, collectionName, toInsert)
			if tc.errorHappens {
				ts.Error(err)
			} else {
				ts.NoError(err)
			}
			if err == nil {
				ts.Equal(tc.expResult, count, "inequality")
			}

			//TODO: check what is actually in the database

			ts.clean()
		})
	}
}

func (ts *TestSuite) insert(doc ModelTest) {
	_, err := ts.repo.db.Collection(collectionName).InsertOne(context.Background(), doc)
	ts.NoError(err)
}

func (ts *TestSuite) clean() {
	_, err := ts.repo.db.Collection(collectionName).DeleteMany(context.Background(), bson.M{})
	ts.NoError(err)
}
