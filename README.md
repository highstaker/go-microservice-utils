

## TODO

- [x] Create config
- [ ] Create servers
    - [x] Create basic dummy server
    - [x] Create GRPC server
    - [ ] Create HTTP server
    - [ ] Create probe server
    - [ ] Create metrics server
    
- [ ] Check with linter
