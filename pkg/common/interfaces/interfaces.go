package interfaces

import "context"

type Logger interface {
	Info(v ...any)
	Infof(format string, v ...any)

	Error(v ...any)
	Errorf(format string, v ...any)

	Fatal(v ...any)
	Fatalf(format string, v ...any)

	WithErr(err error) Logger
}

type Server interface {
	Start() error
	Stop() error

	// If true, server failure to start causes the entire app to shut down.
	// If false, just log and continue.
	GetObligatoryStart() bool

	GetName() string
}

type MongoClient interface {
	GetOne(ctx context.Context, collectionName string, destStruct interface{}, filter ...MongoClientFilter) error
	GetCount(ctx context.Context, collectionName string, filters ...MongoClientFilter) (int64, error)
	Insert(ctx context.Context, collectionName string, docsToInsert []interface{}) (int64, error)

	IsErrNotFound(err error) bool
}
