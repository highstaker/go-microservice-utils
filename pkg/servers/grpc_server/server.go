package grpcserver

import (
	"fmt"
	"net"

	commonInterfaces "gitlab.com/highstaker/go-microservice-utils/pkg/common/interfaces"
	"gitlab.com/highstaker/go-microservice-utils/pkg/servers/config"
	"google.golang.org/grpc"
)

const (
	ServerNameGRPC = "GRPC Server"
)

type ServerGetter interface {
	GetServer() *grpc.Server
}

type GrpcServer struct {
	server *grpc.Server
	config *GrpcServerConfig

	logger commonInterfaces.Logger
}

type GrpcServerConfig struct {
	Server  *config.ServerConfig
	Options []grpc.ServerOption
}

func New(cfg *config.ServerConfig, logger commonInterfaces.Logger) *GrpcServer {
	// TODO: options. Particularly, connectionTimeout. Also, interceptors.
	var options []grpc.ServerOption

	config := &GrpcServerConfig{
		Server:  cfg,
		Options: options,
	}

	srv := &GrpcServer{
		server: grpc.NewServer(options...),
		config: config,
		logger: logger,
	}

	return srv
}

func (s *GrpcServer) Start() error {
	l, err := net.Listen("tcp", s.config.Server.Host+":"+s.config.Server.Port)
	if err != nil {
		return fmt.Errorf("failed to create listener: %w", err)
	}

	go func() {
		s.logger.Infof("GRPC server started on address %v:%v", s.config.Server.Host, s.config.Server.Port)
		err := s.server.Serve(l)
		if err != nil {
			s.logger.WithErr(err).Error("GRPC server Serve failed")
		}
	}()

	return nil
}

func (s *GrpcServer) Stop() error {
	//TODO

	return nil
}

func (s *GrpcServer) GetObligatoryStart() bool {
	return s.config.Server.Obligatory
}

func (s *GrpcServer) GetName() string {
	return ServerNameGRPC
}

func (s *GrpcServer) GetServer() *grpc.Server {
	return s.server
}
